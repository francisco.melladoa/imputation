
# Importacion de modulos
from time import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import psycopg2 as pg
# Definicion de variables de identificacion para coneccion a base de datos.
import argparse

def parse_args():

    parser = argparse.ArgumentParser(description='script para subir la data de los solcats')

    parser.add_argument('-host', action='store', dest='HOST',
                        help='ip of db host')
    parser.add_argument('-usr', action='store', dest='USER',
                        help='username of db')
    parser.add_argument('-pwd', action='store', dest='PASSWORD',
                        help='pwd of db')
    parser.add_argument('-prt', action='store', dest='PORT',
                        help='port of db')
    parser.add_argument('-db', action='store', dest='DATABASE',
                        help='database name')
    parser.add_argument('-idcliente', action='store', dest='IDCLIENTE',
                        help='identificador cliente')

    args = parser.parse_args()
    
    return args

args = parse_args()
pg_host = args.HOST
pg_db = args.DATABASE
pg_user = args.USER
pg_pwd = args.PASSWORD
pg_port = args.PORT

print(pg_host,pg_user,pg_pwd)

## llamado asi:  python test_params.py -host a -usr b -pwd c 



def BD(pg_host,pg_db,pg_user,pg_pwd,pg_port):
    '''

    Coneccion a base de datos
    '''
    pg_conn = pg.connect(host=pg_host,dbname=pg_db,user=pg_user,password=pg_pwd,  port=pg_port)
    pg_cur = pg_conn.cursor()
    return pg_cur, pg_conn
def authorized():
    # Obtencion de diccionario con id de plantas autorizadas para imputacion
    cur, conn = BD(pg_host,pg_db,pg_user,pg_pwd,pg_port)
    query_plantas = """SELECT planta.id_p
					FROM planta
					WHERE planta.activo_p = TRUE
						AND planta.id_c <> 0
						AND planta.imputar_datos_planta = TRUE"""
						#AND planta.fecha_proc_diario_planta < ('""" + fecha_hora_utc + """' at time zone timezone_planta)::date"""
    cur.execute(query_plantas)
    plantas = cur.fetchall()
    ids_p = list()
    for ii in range(len(plantas)):
        id_p = int(plantas[ii][0]); ids_p.append(id_p)
    return ids_p

def ID_P(cl):
    '''
    Funcion de llamada de datos para planta y dia especificos
    '''
    # Obtencion de diccionario con id de plantas para cada cliente
    pg_cur, pg_conn = BD(pg_host,pg_db,pg_user,pg_pwd,pg_port)

    # Definicion de parametros a usar en la query para la llamada de datos.
    #cliente = cl
    # Query de adquisicion de datos remota de los inversores para una planta y dia especificos.
    query = """
    SELECT cliente.id_c,
        planta.id_p,
        planta.nombre_p,
        planta.periodicidad_segundos_p
    FROM planta
    JOIN cliente ON cliente.id_c = planta.id_c
    WHERE planta.activo_p = TRUE
        AND planta.id_c <> 0
    ORDER BY planta.id_c,
        planta.nombre_p;
    """#%(cliente)
    # Llamada de datos mediante query
    pg_cur.execute(query)
    results = pg_cur.fetchall()
    df0 = pd.DataFrame(results)
    pd.set_option('mode.chained_assignment', None) # Para evitar mensaje de error
    df = {}
    cls_unicos = np.unique( np.array(df0[0]) )
    for cliente in cls_unicos:
            df_tmp1 = df0.loc[ df0[0] == cliente]
            df_tmp1.drop(0, axis=1, inplace=True)
            df_tmp2 = df0.loc[ df0[3] == cliente] 
            df[cliente] = list(df_tmp1[1].values)
    id_p_cliente = df[cl]
    return id_p_cliente
def miss_count(client, start, finish):
    '''
    
    Funcion de conteo y calculo de metrica para datos faltantes en todas las plantas para un cliente
    
    
    
    
    '''
    cliente = client
    a = ID_P(cliente)
    miss_summary = pd.DataFrame()
    miss_client_dict = {}
    for id_p in a:
        # Control de excepcion para planta con 
        if id_p ==67:
            None
        else:
            init = start
            end = finish
            b = CALL(int(id_p),init,end)
            power, index, cols, ind  = NONE (b)
            miss = power.isnull().values.any()
            # Filtrado de plantas que cuya data se encuentra completa 
            c = 0
            d = 0
            e = 0
            dates = []
            for pw in b[4]:
                if pw == None:
                    date = str(b[3][d])
                    dates.append(date)
                    #print(str(c) + '   ' + str(date))
                    c = c + 1
                d = d + 1
            if miss == True:
                tmp = power.isnull().sum().sort_values(ascending =False)
                miss_percent = tmp*100/len(power)
                miss_overall = tmp*100/(len(power)*len(tmp))
                tmp = pd.DataFrame(tmp)
                a = np.zeros(len(cols))
                b = np.where(a==0,str(id_p), a)
                miss_resume = pd.DataFrame()
                miss_resume['id_p'] = b
                miss_resume['id_i'] = tmp.index
                miss_resume['missing_data_count'] = tmp.values
                miss_resume['inversor_%_missing_data_'] = miss_percent.values
                miss_resume['overall_%_missing_data_'] = miss_overall.values
                power_miss = power.copy()
                miss_summary = pd.concat([miss_summary, miss_resume])
                miss_plant_dict = {}
                for id_i in cols:
                    miss_list = []
                    inv_power = power_miss[id_i]
                    data_len = len(inv_power)
                    s = inv_power.isna().groupby(inv_power.notna().cumsum()).sum()
                    for x in s.index:
                        if s[x] != 0:
                            count = s[x]
                            periods = []
                            for i in range(count):
                                period = dates[e]# list(power_miss.index[x:count+x]
                                periods.append(period)
                                e = e + 1
                            percent = count*100/data_len
                            miss_inv = count, percent, periods
                            miss_list.append(miss_inv) 
                        else:
                            None
                    miss_plant_dict[id_i] = miss_list
                # Despliegue de visuaizacion de datos faltantes para cada planta
                #fig = sns.heatmap(power.isnull(), cbar=False, xticklabels=True,cmap = ['darkorange', 'Red'], linewidths = 0.0001 )
                #fig.set_title(cliente+' - Planta N° '+str(id_p),fontsize=14)
                #fig.set_xlabel("Inversor Serial Number ID")
                #image = fig.plot()
                #image_name = message+'.png'
                #plt.savefig(message)
                miss_client_dict[id_p] = miss_plant_dict
            else:
                None
    return miss_client_dict,  miss_summary
from typing import Union

def fill_with_hard_limit(
        df_or_series: Union[pd.DataFrame, pd.Series], limit: int,
        fill_method='interpolate',
        **fill_method_kwargs) -> Union[pd.DataFrame, pd.Series]:
    """The fill methods from Pandas such as ``interpolate`` or ``bfill``
    will fill ``limit`` number of NaNs, even if the total number of
    consecutive NaNs is larger than ``limit``. This function instead
    does not fill any data when the number of consecutive NaNs
    is > ``limit``.
    :param df_or_series: DataFrame or Series to perform interpolation
        on.
    :param limit: Maximum number of consecutive NaNs to allow. Any
        occurrences of more consecutive NaNs than ``limit`` will have no
        filling performed.
    :param fill_method: Filling method to use, e.g. 'interpolate',
        'bfill', etc.
    :param fill_method_kwargs: Keyword arguments to pass to the
        fill_method, in addition to the given limit.

    :returns: A filled version of the given df_or_series according
        to the given inputs.
    """

    # Keep things simple, ensure we have a DataFrame.
    try:
        df = df_or_series.to_frame()
    except AttributeError:
        df = df_or_series

    # Initialize our mask.
    mask = pd.DataFrame(True, index=df.index, columns=df.columns)

    # Get cumulative sums of consecutive NaNs.
    grp = (df.notnull() != df.shift().notnull()).cumsum()

    # Add columns of ones.
    grp['ones'] = 1

    # Loop through columns and update the mask.
    for col in df.columns:

        mask.loc[:, col] = (
                (grp.groupby(col)['ones'].transform('count') <= limit)
                | df[col].notnull()
        )

    # Now, interpolate and use the mask to create NaNs for the larger
    # gaps.
    method = getattr(df, fill_method)
    out = method(limit=limit, **fill_method_kwargs)[mask]

    # Be nice to the caller and return a Series if that's what they
    # provided.
    if isinstance(df_or_series, pd.Series):
        # Return a Series.
        return out.loc[:, out.columns[0]]

    return out
def CALL(id_p,init,end):
    '''
    Funcion de llamada de datos para planta y dia especificos
    '''
    # Definicion de parametros a usar en la query para la llamada de datos.
    id_planta = id_p
    fecha_init = init
    fecha_end = end
    pg_cur, pg_conn = BD(pg_host,pg_db,pg_user,pg_pwd,pg_port)
    # Query de adquisicion de datos remota de los inversores para una planta y dia especificos.
    query = """
    WITH void_time_inv AS (
        SELECT x::time AS tiempo
        FROM planta
        CROSS JOIN generate_series(current_date, current_date + '1 day - 1 second'::interval, concat(planta.periodicidad_segundos_p::text, ' seconds')::interval) AS t(x)
        WHERE planta.id_p = %s--@id_planta
    ), fecha_inversor AS (
        SELECT '%s'::date--@fecha_desde
            + void_time_inv.tiempo AS fecha, inversor.id_i, planta.timezone_planta, planta.id_p, planta.nombre_p, inversor.serial_number_i
        FROM void_time_inv
        CROSS JOIN inversor
        JOIN planta ON planta.id_p = inversor.id_p
        WHERE inversor.id_p = %s--@id_planta
    ), potencia_inversor_original AS (
        SELECT date_round(inversor_medicion.fecha_im, (planta.periodicidad_segundos_p::text || ' seconds')::interval) AT TIME ZONE 'UTC' AS fecha_im,
            inversor_medicion.active_power_im,
            inversor_medicion.ac_current_phase1_im,
            inversor_medicion.ac_current_phase2_im,
            inversor_medicion.ac_current_phase3_im,
            inversor_medicion.ac_voltage_phase1_im,
            inversor_medicion.ac_voltage_phase2_im,
            inversor_medicion.ac_voltage_phase3_im,
            inversor_medicion.id_i
        FROM inversor_medicion
        JOIN planta ON planta.id_p = inversor_medicion.id_p
        WHERE inversor_medicion.id_p = %s--@id_planta
            AND inversor_medicion.fecha_im >= '%s'::timestamp--@fecha_desde
            AND inversor_medicion.fecha_im < '%s'::timestamp--@fecha_hasta
    )
    SELECT fecha_inversor.nombre_p,
        fecha_inversor.id_i,
        fecha_inversor.serial_number_i,
        fecha_inversor.fecha,
        potencia_inversor_original.active_power_im
        --potencia_inversor_original.ac_current_phase1_im,
        --potencia_inversor_original.ac_current_phase2_im,
        --potencia_inversor_original.ac_current_phase3_im,
        --potencia_inversor_original.ac_voltage_phase1_im,
        --potencia_inversor_original.ac_voltage_phase2_im,
        --potencia_inversor_original.ac_voltage_phase3_im
    FROM fecha_inversor
    JOIN planta_localidad_gen ON planta_localidad_gen.id_planta = fecha_inversor.id_p
    JOIN horario_gen ON horario_gen.id_localidad_gen = planta_localidad_gen.id_localidad_gen
        AND horario_gen.fecha_horario_gen = '%s'--@fecha_desde
    LEFT JOIN potencia_inversor_original ON potencia_inversor_original.fecha_im = fecha_inversor.fecha
        AND potencia_inversor_original.id_i = fecha_inversor.id_i
    WHERE fecha_inversor.fecha BETWEEN horario_gen.fecha_horario_gen + horario_gen.inicio_horario_gen AND horario_gen.fecha_horario_gen + horario_gen.fin_horario_gen
        --AND fecha_inversor.id_i = 189 --@id_inversor
    ORDER BY 1, 2, 4;
    """%((id_planta,fecha_init,id_planta,id_planta,fecha_init,fecha_end,fecha_init))
    # Llamada de datos mediante query
    pg_cur.execute(query)
    results = pg_cur.fetchall()
    df = pd.DataFrame(results)
    return df
def NONE(raw_power):
    '''
    Reordenamiento preliminar de datos faltantes

    '''
    # Creacion de indice temporal
    time_index = pd.DataFrame(raw_power[3].drop_duplicates())
    time_index.set_axis(['Time'],axis = 'columns', inplace=True)
    index = time_index['Time']
    ind = pd.to_datetime(index, unit='s')
    c = 0
    for i in ind:
        now = i
        current_time = now.strftime("%Y-%m-%d %H:%M:%S")
        ind[c] = current_time
        c = c + 1
    # Creacion de datasets individuales tipo DF con informacion de potencia con todos los inversores para la planta en cuestion
    power_tmp = raw_power[[1,3,4]]
    power_tmp.columns = ['id_i', 'date_time', 'active_power_im']
    '''
    Funcion de separacion de datos por inversor. Entrega un diccionario donde cada key coresponde al numero de inversor
    y el argumento corresponde a los datos de potencia.

    df = Dataframe con datos de ID de inversor y otros.

    '''
    pd.set_option('mode.chained_assignment', None)
    df = power_tmp.copy()
    df_separado = {}
    ids_unicos = np.unique( np.array(df['id_i']) )
    for id_i in ids_unicos:
        df_tmp = df.loc[ df['id_i'] == id_i ]
        df_tmp.drop('id_i', axis=1, inplace=True)
        df_separado[id_i] = list(df_tmp['active_power_im'].values)
    inv = df_separado
    df0 = pd.DataFrame(inv).set_index(ind, inplace = False)
    power0 = df0.apply(pd.to_numeric, downcast='float')
    cols0 = list(power0.columns)
    return power0, index, cols0, ind
def imput(pl, cl, init, end):
    '''

    '''
    percent_limit = pl
    Ids_p = ID_P(cl)
    miss_summary = pd.DataFrame()
    plant_dict = {}
    au = authorized()
    ids_p = []
    for i in au:
        if (i not in ids_p) and (i in Ids_p):
            ids_p.append(i)
    for id_p in ids_p:
        if id_p ==67:
            print('CLient ID: '+str(cl) + ' - Plant ID: '+ str(id_p)+' Check Data Base !!!')
        else:
            miss_description, summary = miss_count(cl,init,end)
            message = 'CLient ID: '+str(cl) + ' - Plant ID: '+ str(id_p)
            print(message)
            raw_power = CALL(int(id_p),init,end)
            if raw_power.shape==(0,0):
                print('THERE IS NO DATA !')
                continue 
            else:
                power, index, cols, ind  = NONE (raw_power)
                miss = power.isnull().values.any()
                # Filtrado de plantas que cuya data se encuentra completa 
                if miss == True:
                    limit = int(len(ind)*percent_limit)+1
                    inv_data = list()
                    for id_i in cols:
                        if len(miss_description[id_p][id_i]) == 0: 
                            print('    Inversor ID '+str(id_i)+': Complete Data')
                        elif miss_description[id_p][id_i][0][0] <= limit:
                            inv_power = power.copy()
                            power_filled_interpolate =  pd.DataFrame(fill_with_hard_limit(inv_power,limit, fill_method='interpolate') )
                            power_filled_bfill =  pd.DataFrame(fill_with_hard_limit(power_filled_interpolate,limit, fill_method='bfill') )
                            imputed_data = power_filled_bfill
                            for i in range(len(miss_description[id_p][id_i])):
                                date_times = miss_description[id_p][id_i][i][2]
                                for time in date_times:
                                    power_time = imputed_data[id_i][time]
                                    data = [id_i, time, power_time]
                                    inv_data.append(data)
                            #power_filled_bfill.plot()
                            if imputed_data[id_i].isnull().values.any() == False:
                                print('    Inversor ID '+str(id_i)+': Succefull Data Imputation')
                            else:
                                print('    Inversor ID '+str(id_i)+': Check Imputation Method')
                        else:
                            print('    Inversor ID '+str(id_i)+': Exceed imputation limit ('+str((miss_description[id_p][id_i][0][1]))+' %)')

                            '''
                            import numpy as np
                            from sklearn.experimental import enable_iterative_imputer
                            from sklearn.impute import IterativeImputer
                            train = [power_filled_bfill['30214129']]
                            test = [power_filled_bfill['30164848']]
                            imp = IterativeImputer(max_iter=10, random_state=0)
                            imp.fit(train)
                            imputed_iter = pd.DataFrame(imp.transform(test).T)
                            imputed_iter.plot()
                            power_filled_bfill['30164848']=imputed_iter.values
                            print(power_filled_bfill)
                            '''
                        plant_dict[id_p] = inv_data
                else: 
                    print('    No missing data !')
    return plant_dict
def IMPUTE(clients, init, end, pl = 0.15):
    '''

    '''
    print('Imputation algorithm - DateTime: '+init)
    total_impute = {}
    for client in clients:
        ti = time()
        imputed = imput(pl, client, init, end)
        total_impute[client] = imputed
        tf = time();t = round((tf-ti),2); print('    Processing time: '+str(t)+' s'); print('')
    return total_impute

clients = args.IDCLIENTE # (2,6,7,10,12,13,14,15,16,17)
clients = [int(client) for client in clients.split(',')]
print('Lista de clientes: '+str(clients))

#fecha_init = pd.to_datetime("today").strftime("%Y-%m-%d")
#fecha_end = (pd.to_datetime("today")+pd.Timedelta('1D')).strftime("%Y-%m-%d")
imputation_values = IMPUTE(clients, init = '2021-05-06', end = '2021-05-07', pl = 0.15)
pg_conn = None
pg_cur, pg_conn = BD(pg_host,pg_db,pg_user,pg_pwd,pg_port)
for i in imputation_values:
    for j in imputation_values[i]:
        imput_list = imputation_values[i][j]
        inv = pd.DataFrame(imput_list)
        inv = inv.drop_duplicates()
        for k in range(len(inv)):
            imput_value = list(inv.iloc[k,:])
            imput_value[2] = float(imput_value[2]) 
            imput_value[0] = int(imput_value[0]) 
            print(imput_value)
            pg_cur.callproc('ins_medicion_potencia_imputed', imput_value)
            pg_conn.commit()
            results = pg_cur.fetchall()
            print(results)